<?php
return [
    //Server Enviroments
    'connections' => [
        'login' => [
            'uri' => env('IDENTITY_SERVER'),
            'clientId' => env('IDENTITY_SERVER_USERNAME'),
            'clientSecret' => env('IDENTITY_SERVER_PASSWORD'),
            'scope' => env('IDENTITY_SERVER_SCOPE'),
            'grantType' => 'client_credentials',
        ],
        'production' => [
            'uri' => env('PRODUCTION_SERVER'),
            'username' => env('PRODUCTION_SERVER_USERNAME'),
            'password' => env('PRODUCTION_SERVER_PASSWORD'),
            'scope' => env('PRODUCTION_SERVER_SCOPE'),
        ],
        'test' => [
            'uri' => env('TEST_SERVER'),
            'username' => env('TEST_SERVER_USERNAME'),
            'password' => env('TEST_SERVER_PASSWORD'),
            'scope' => env('TEST_SERVER_SCOPE'),
        ]
    ]
];
