<?php

use App\Http\Livewire\Admin\Ax2012;
use App\Http\Livewire\Admin\Dashboard;
use App\Http\Livewire\Admin\FlexReport;
use App\Http\Livewire\Admin\Redmine;
use App\Http\Livewire\Admin\ShowAnalyse;
use App\Http\Livewire\Admin\ShowUsers;
use App\Http\Livewire\Admin\SyncUpload;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
     'prefix' => 'admin',
    'middleware' => 'auth:sanctum'
], function () {
    //Dashboard
    Route::get('/dashboard',Dashboard::class)->name('dashboard');
    // Users
    Route::get('/users', ShowUsers::class)->name('users.index');
    Route::get('/analyse', ShowAnalyse::class)->name('analyse.index');
    Route::get('/redmine', Redmine::class)->name('redmine.index');
    Route::get('/ax2012', Ax2012::class)->name('ax2012.index');
    Route::get('/flexreport', FlexReport::class)->name('sync.index');
});


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', Dashboard::class)->name('dashboard');
