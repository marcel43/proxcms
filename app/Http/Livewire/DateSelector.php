<?php

namespace App\Http\Livewire;

use App\Models\OutputExport;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class DateSelector extends Component
{
    public $today;
    public $dateSet;
    public $month;
    public $year;
    public $calendar;

    public function mount()
    {
        $today = Carbon::now();
        $this->today = $today->format('l jS \\of F Y');
        $this->setDate(Carbon::now()->format('Y-m-d'));
    }

    /**
     * @return Application|Factory|View
     */
    public function render()
    {
        return view('livewire.date-selector');
    }

    public function decreaseMonth()
    {
        $date = Carbon::parse($this->dateSet)->subMonth();
        $this->setDate($date);
    }

    public function increaseMonth()
    {
        $date = Carbon::parse($this->dateSet)->addMonth();
        $this->setDate($date);
    }

    public function dataSelect($weekIndex, $dayIndex)
    {
        $date = $this->calendar[$weekIndex]['days'][$dayIndex]['date'];
        $this->dateSet = $date;
        $this->emit('dateChange', ['date' => $date]);
    }

    private function setDate($date) {
        $date = Carbon::parse($date);
        $this->dateSet = $date->format('Y-m-d');
        $this->month = $date->format('F');
        $this->year = $date->format('Y');
        $this->calendar = $this->setWeekNo($date);
    }

    /**
     * @param Carbon $date
     */
    private function setWeekNo(Carbon $date) {
        $startDate = $date->startOfMonth();
        $startWeek = $startDate->weekOfYear;
        if($startWeek == 52) {
            $startWeek = 1;
        }
        $endWeek = $date->endOfMonth()->weekOfYear;
        $dates = [];
        while ($startWeek <= $endWeek) {
                $week = [
                    'number' => $startWeek,
                    'days' => $this->getweekDates($startWeek, $date->year)
                ];
                $dates[] = $week;
                $startWeek++;

        }
        return $dates;
    }

    private function getweekDates($week, $year) {
        $date = Carbon::now();
        $date->setISODate($year,$week);
        $startDate = $date->startOfWeek()->format('Y-m-d');
        $endDate = $date->endOfWeek()->format('Y-m-d');
        $days = [];
        while($startDate <= $endDate) {
            $days[] = [
                'date' => $startDate,
                'isCurrent' => ($startDate === Carbon::now()->format('Y-m-d')),
                'hasData' => $this->hasData($startDate),
                'isSelected' => false,
                'isWeekend' => Carbon::parse($startDate)->isWeekend()
            ];
            $startDate = Carbon::parse($startDate)->addDay()->format('Y-m-d');
        }
        return $days;
    }

    private function hasData($date){
      return OutputExport::whereDate('DateTimeXml', $date)->exists();
    }
}
