<?php

namespace App\Http\Livewire\Dashboard;

use App\Models\KprUser;
use Illuminate\Support\Carbon;
use Livewire\Component;

class Table extends Component
{
    public $byweek = false;

    public function mount()
    {

    }

    public function render()
    {
        return view('livewire.dashboard.table', [
            'data' => $this->generateGraphData($this->getData())
        ]);
    }

    private function getData() {
        if($this->byweek) {
            return KprUser::where('DATE_ACTIVATION', '>=',
                Carbon::now()->subMonths(3)
                    ->format('Y-m-d')
            )
                ->groupBy('WEEK_ACTIVATION')
                ->selectRaw('WEEK(DATE_ACTIVATION) AS WEEK_ACTIVATION')
                ->get();
        }
        return KprUser::where('DATE_ACTIVATION', '>=',
                Carbon::now()->subDays(7)
                ->format('Y-m-d')
            )
            ->groupBy('DATE_ACTIVATION')->get();
    }

    private function generateGraphData($items) {
        $data = [];
        foreach ($items as $item) {
            if ($this->byweek) {
                $raw = [];
                $raw['datum'] = $item->WEEK_ACTIVATION;
                $raw['aantal'] = KprUser::
                whereRaw('WEEK(DATE_ACTIVATION) < '. $item->WEEK_ACTIVATION)
                    ->count();
                $data[] = $raw;
            }else {
                $raw = [];
                $raw['datum'] = $item->DATE_ACTIVATION;
                $raw['aantal'] = KprUser::
                where('DATE_ACTIVATION', '<=', $item->DATE_ACTIVATION)
                    ->count();
                $data[] = $raw;
            }

        }
        return $data;
    }
}
