<?php

namespace App\Http\Livewire\Dashboard;

use App\Models\KprUser;
use Asantibanez\LivewireCharts\Facades\LivewireCharts;
use Asantibanez\LivewireCharts\Models\ColumnChartModel;
use Asantibanez\LivewireCharts\Models\LineChartModel;
use Illuminate\Support\Carbon;
use Livewire\Component;

class Chart extends Component
{

    public $firstRun = true;

    public $byweek = false;

    public $numbers = 7; //default setting is one week

    public $showDataLabels = false;



    protected $listeners = [
        'onPointClick' => 'handleOnPointClick',
        'onSliceClick' => 'handleOnSliceClick',
        'onColumnClick' => 'handleOnColumnClick',
    ];
    public function render()
    {
        if($this->byweek)
            $this->firstRun = true;

        return view('livewire.dashboard.chart', [
            'columnChartModel' => $this->buildChart($this->generateGraphData($this->getData())),
        ]);
    }

    private function getData() {
        if($this->byweek) {
            return KprUser::where('DATE_ACTIVATION', '>=',
                Carbon::now()->subMonths(3)
                    ->format('Y-m-d')
            )
                ->groupBy('WEEK_ACTIVATION')
                ->selectRaw('WEEK(DATE_ACTIVATION) AS WEEK_ACTIVATION')
                ->get();
        }
        return KprUser::where('DATE_ACTIVATION', '>=',
            Carbon::now()->subDays($this->numbers)
                ->format('Y-m-d')
        )
            ->groupBy('DATE_ACTIVATION')->get();
    }

    private function generateGraphData($items) {
        foreach ($items as $item) {
            if ($this->byweek) {
                $date = \Carbon\Carbon::now();
                $date = $date->setISODate(Carbon::now()
                    ->format('Y'), $item->WEEK_ACTIVATION)->endOfWeek()->format('Y-m-d');
                if ($date >= \Carbon\Carbon::now()->format('Y-m-d')) {
                    $date = \Carbon\Carbon::now()->format('Y-m-d');
                }

                $item->amount = KprUser::where('DATE_ACTIVATION', '<=', $date
                )
                    ->count();

            }else {
                $item->amount = KprUser::
                where('DATE_ACTIVATION', '<=', $item->DATE_ACTIVATION)
                    ->count();
            }
        }

        return $items;
    }

    private function buildChart($chartData) {
        $xAxis = [];
        $lineChartModel = $chartData
            ->reduce(function ($lineChartModel, $data) use ($chartData) {
                if($this->byweek) {

                    $index = $data->WEEK_ACTIVATION;
                }else {
                    $index = $data->DATE_ACTIVATION;
                }

                $xAxis[$index] = $index;
                $amountSum = $data->amount;

                $lineChartModel->addMarker($index, $amountSum);
                return $lineChartModel->addPoint($index, $data->amount, ['id' => $this->byweek? $data->WEEK_ACTIVATION:$data->DATE_ACTIVATION]);
            }, LivewireCharts::lineChartModel()
                ->setTitle($this->byweek? 'Account growth over last '.count($chartData).' weeks':'Account Numbers over last '.$this->numbers.' days')
                ->setAnimated($this->firstRun)
//                ->withOnPointClickEvent('onPointClick')
                ->withLegend()
                ->setColors(['#ff8c00', 'gray'])
                ->setGridVisible(true)
            );
            $this->firstRun = false;

           // dd($lineChartModel);
        return $lineChartModel;
    }
}
