<?php

namespace App\Http\Livewire\Admin;

use Aareon\Ax2012\Client\Ax2012RestAdapter;
use Livewire\Component;

class Ax2012 extends Component
{
    public function render()
    {
        $conn = new Ax2012RestAdapter();
        $conn->getApi('login');
        dd([$conn, config('ax2012')]);
        return view('livewire.admin.ax2012');
    }
}
