<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class FlexReport extends Component
{
    public $title = 'test';

    public function render()
    {
        return view('livewire.admin.flex-report');
    }

}
