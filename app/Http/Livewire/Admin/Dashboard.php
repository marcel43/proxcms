<?php


namespace App\Http\Livewire\Admin;


use App\Models\AareonLog;
use Asantibanez\LivewireCharts\Facades\LivewireCharts;
use Livewire\Component;

class Dashboard extends Component
{
    public function render()
    {
        //dd(AareonLog::getAVGDayReport());
        return view('dashboard', [
            'avgPerHourChart' => $this->chartAvgPerHour(AareonLog::getAVGHourReport()),
            'avgPerDayChart' => $this->chartAvgPerDay(AareonLog::getAVGDayReport())
        ]);
    }


    /**
     * Returns a Chart for AVG per hour overview
     *
     * @param $LogReport
     * @return LivewireCharts
     */
    private function chartAvgPerHour($LogReport)
    {
        return $LogReport
            ->reduce(function ($lineChartModel, $data) use ($LogReport) {
                $index = $LogReport->search($data);

                return $lineChartModel
                    ->addSeriesPoint($data->the_message, $index, $data->amount, ['id' => $data->the_hour.'h']);
            }, (LivewireCharts::multiLineChartModel()
                ->setTitle('Gemiddelde registratie aanvragen per uur')
                ->setAnimated(true)
                ->withOnPointClickEvent('onPointClick')
                ->setSmoothCurve()
                ->setLegendVisibility(true)
                ->setXAxisVisible(true)
                ->setYAxisVisible(true)
                ->setDataLabelsEnabled(false)
                ->sparklined())
            );
    }

    private function chartAvgPerDay($LogReport)
    {
        return $LogReport
            ->reduce(function ($lineChartModel, $data) use ($LogReport) {
                $index = $LogReport->search($data);

                return $lineChartModel
                    ->addSeriesPoint($data->the_message, $data->the_day, $data->amount, ['id' => $data->the_day]);
            }, (LivewireCharts::multiLineChartModel()
                ->setTitle('Gemiddelde registratie aanvragen per dag')
                ->setAnimated(true)
                ->withOnPointClickEvent('onPointClick')
                ->setSmoothCurve()
                ->setXAxisVisible(true)
                ->setDataLabelsEnabled(false)
                ->setColors(['#b01a1b', '#d41b2c', '#ec3c3b', '#f66665'])
                ->withLegend()
                ->withGrid()
                ->sparklined()
            )
            );
    }
}
