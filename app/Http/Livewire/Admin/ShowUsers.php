<?php

namespace App\Http\Livewire\Admin;

use App\Http\Resources\Admin\UserCollection;
use App\Models\User;
use Illuminate\Http\Request;
use Livewire\Component;
use Livewire\WithPagination;


class ShowUsers extends Component
{
    use WithPagination;

    public $search = '';
    public $name = '';
    public $email=  '';


    public function render()
    {
        //Setting the fields to display.
        user::setStaticVisible([
            'id',
            'name',
            'email',
            'created_at',
            'email_verified_at',
            'two_factor_secret'
        ]);

        return view('livewire.admin.show-users',[
            'resources' => new UserCollection(User::FilterBy($this->parameters())->paginate(15)),
            'filters' => User::filterNames(),
            'columns' => User::getStaticVisible(),
        ]);
    }

    private function parameters()
    {
        return [
            'search' => $this->search,
            'email' => $this->email,
            'name' => $this->name
        ];
    }
}
