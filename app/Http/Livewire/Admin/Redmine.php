<?php

namespace App\Http\Livewire\Admin;

use App\Repositories\Redmine\RedmineIssueRepository;
use Livewire\Component;

class Redmine extends Component
{
    public $issue;

    public function mount(RedmineIssueRepository $issue) {
        $this->issue = $issue;
    }

    public function render()
    {
        dd($this->issue->all());
        return view('livewire.admin.redmine');
    }
}
