<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class SyncUpload extends Component
{
    public function render()
    {
        return view('livewire.admin.sync-upload');
    }
}
