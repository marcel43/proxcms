<?php

namespace App\Http\Livewire\Actions;

use App\Imports\SyncExport;
use App\Imports\SyncImport;
use App\Models\Admin\UploadedFiles;
use App\Models\OutputExport;
use App\Models\OutputImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;

class UploadImport extends Component
{
    use WithFileUploads;

    public $import;
    public $canAcces = true;
    public $processing = false;
    public $oldCountExport = 0;
    public $oldCountImport = 0;

    public function render()
    {
        $this->setCounters();
        return view('livewire.actions.upload-import');
    }

    public function save()
    {
        $this->processing = true;
        $this->validate([
            'import' => 'file|max:100400'
        ]);
        $file = $this->import->store('temp');

        $this->fileDetails($file);
        //first we count numbers of rows
        if($this->ColumnCount($file) > 3) {
            Excel::import(new SyncImport, $file);
        }else {
            Excel::import(new SyncExport, $file);
        }
        $this->emit('fileDone');
        return back();

    }

    private function setCounters()
    {
        $this->oldCountExport = OutputExport::count();
        $this->oldCountImport = OutputImport::count();
    }

    private function fileDetails($file)
    {
        UploadedFiles::create([
            'filename' => $this->import->getClientOriginalName(),
            'records' => $this->RecordCount($file)
        ]);
    }

    private function ColumnCount($file)
    {
        return Excel::toCollection(
            new SyncExport, $file,'local', \Maatwebsite\Excel\Excel::TSV
        )->first()->first()->count();
    }

    private function RecordCount($file)
    {
        return Excel::toCollection(
            new SyncExport, $file,'local', \Maatwebsite\Excel\Excel::TSV
        )->first()->count();
    }
}
