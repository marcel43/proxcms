<?php

namespace App\Http\Livewire\Report;

use App\Models\OutputExport;
use App\Models\OutputImport;
use Livewire\Component;
use phpDocumentor\Reflection\Types\Array_;

class FlexStatus extends Component
{
    /**
     * @var $data
     */
    public $data = [];

    protected $listeners = ['fileDone' => 'reloadData'];

    public function mount()
    {
        $this->data =  $this->getData();

    }
    public function render()
    {
        return view('livewire.report.flex-status');
    }

    public function getData()
    {
        $data = [];
        $data[0] = [
            'name' => 'OutputImport',
            'count' => OutputImport::count()
        ];
        $data[1] = [
            'name' => 'OutputExport',
            'count' => OutputExport::count()
        ];
        return $data;
    }

    public function reloadData() {
        $this->data =  $this->getData();
    }
}
