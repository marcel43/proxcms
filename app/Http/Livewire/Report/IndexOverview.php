<?php

namespace App\Http\Livewire\Report;

use App\Models\OutputExport;
use App\Models\OutputImport;
use Asantibanez\LivewireCharts\Facades\LivewireCharts;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use phpDocumentor\Reflection\Types\Collection;

class IndexOverview extends Component
{
    protected $listeners = ['dateChange' => 'changeDates'];

    private $date = "none";
    private $tableInfo;
    private $firstRun = true;
    private $chart;

    public function mount() {
        $this->date = Carbon::now()->subDays(1)->format('Y-m-d');
        $this->changeDates(['date' => $this->date]);
    }

    public function render()
    {
        return view('livewire.report.index-overview', [
            'chart' => $this->chart,
            'tableInfo' => $this->tableInfo,
            'date' => $this->date
        ]);
    }

    /**
     * @param $params
     */
    public function changeDates($params)
    {
        $this->date = $params['date'];
        $data = OutputImport::whereDate('DateTimeXml', $this->date)
            ->ordered();

        $grouped = collect($data)->groupBy('Hour');
        $groupedWithCount = $grouped->map(function($group) {
            return [
                'hour' => $group->first()['hour'],
                'count' => $group->count('SyncTime'),
                'avgSync' => $group->average('SyncTime'),
                'maxSync' => $group->max('SyncTime'),
                'RegAndTimeSync' => $group->avg('SyncTime') + $group->avg('DiffActionXml'),
                'maxRegAndTimeSync' => $group->max('RegAndTimeSync'),
                'avgDiffActionXml' => $group->avg('DiffActionXml'),
            ];
        });
       $this->tableInfo = $groupedWithCount;

       $this->chart = $this->buildChart($groupedWithCount);
    }

    public function getAvgSyncTime($hour) {
        $data = collect(OutputExport::select('UserRefId', 'DateTimeXml')->distinct()
            ->whereRaw('HOUR(DateTimeXml) ='.$hour)->where('TIME(DateTimeXml)', $this->date)
            ->get());
        if(count($data) > 30) {
            dd($data->toArray());
        }
       return count($data);
    }

    private function buildChart($chartData)
    {
        $hours = $chartData->pluck('hour');

        $chart = $chartData
            ->reduce(function ($lineChartModel, $data) use ($chartData) {

                $index = $chartData->search($data);
                return $lineChartModel
                ->addSeriesPoint('ExportXML-ImportXML', $data['hour'], round($data['avgSync']/60,2), ['id' =>  $data['hour']])
                ->addSeriesPoint('ExportHtml-Activatie', $data['hour'], round($data['RegAndTimeSync']/60,2), ['id' =>  $data['hour']]);
            }, LivewireCharts::multiLineChartModel()
                ->setTitle('Duur Activeren Account (in minuten)')
                ->setAnimated($this->firstRun)
                ->withLegend()
                ->setColors(['#ff8c00', 'gray'])
                ->setGridVisible(true)
            );
        $this->firstRun = false;

        return $chart;
    }
}
