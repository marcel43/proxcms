<?php

namespace App\Http\Livewire\Report;

use Livewire\Component;
use \App\Models\Admin\UploadedFiles as Files;

class UploadedFiles extends Component
{
    public $data;

    protected $listeners = ['fileDone' => 'reloadFiles'];

    public function mount()
    {
        $this->data = Files::orderBy('id', 'desc')->take(10)->get();
    }

    public function render()
    {
        return view('livewire.report.uploaded-files');
    }

    public function reloadFiles()
    {
        $this->data = Files::all();
    }
}
