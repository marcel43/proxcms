<?php

namespace App\Http\Livewire;

use App\Models\AareonLog;
use App\Models\AareonLogDay;
use App\Models\KprUser;
use App\Models\KprUserActivation;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class LogReport extends Component
{
    public $byweek = false;

    /**
     * @return Application|Factory|View
     */
    public function render()
    {
        $data = AareonLogDay::all();
        $indexes = [];
        if($this->byweek) {
            $data = AareonLogDay::
                groupBy('week')
                ->selectRaw('
                 WEEK(datum) as week,
                 sum(reg_code_requests) as reg_code_requests,
                 sum(reg_code_requests_unique) as reg_code_requests_unique,
                 sum(reg_code_requests_mail) as reg_code_requests_mail,
                 sum(account_reg_create) as account_reg_create,
                 sum(account_mail_received) as account_mail_received,
                 sum(activated) as activated,
                 sum(not_activated) as not_activated

                 ')->get();
        }
        return view('livewire.log-report', [
            'report' => $data->toArray()
        ]);
    }
}
