<?php

namespace App\Console\Commands;

use App\Models\AareonLog;
use App\Models\AareonLogDay;
use App\Models\KprUser;
use App\Models\KprUserActivation;
use Illuminate\Console\Command;

class ReportDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aareon:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates report table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->prepareData();
        $this->info('Fetching all available Dates');
        $dates = $this->getAllRegAanvragen();
        $progress = $this->output->createProgressBar(count($dates));
        $progress->setMessage('Starting report');
        $progress->start();
        foreach ($dates as $date) {
            AareonLogDay::create([
                'datum' => $date->date,
                'reg_code_requests' => $date->Registratie_aanvragen,
                'reg_code_requests_unique' => $this->getUniekAanvraagCount($date->date),
                'reg_code_requests_mail' => $this->metRegCode($date->date),
                'account_reg_create' => $this->accountAanmaken($date->date),
                'account_mail_received' => $this->countRegistration($date->date),
                'activated' => $this->getActivatedPerDay($date->date),
                'not_activated' => $this->getNotActivatedPerDay($date->date),
            ]);
            $progress->advance();
        }
        $progress->finish();
    }

    private function prepareData()
    {
        //Showing Existing data
        $this->info(AareonLogDay::count() . ' records left');
        //first we truncate the table.
        $this->info('clearing the old data');
        AareonLogDay::query()->truncate();
        $this->info('clearing the old data done');
        //Showing data after truncate
        $this->info(AareonLogDay::count() . ' records left after truncate');
    }

    private function getUniekAanvraagCount($date)
    {
        $registratieAanvragen = '%Get params: {"ReferenceType":"%';
        return AareonLog::select('message')
            ->distinct('message')
            ->where('message', 'LIKE', '%' . $registratieAanvragen . '%')
            ->where('date', $date)
            ->count();
    }

    public function metRegCode($date)
    {
        $registratieAanvragen = '%/api/v0.1/100/Correspondence/Create%';
        return AareonLog::select('message')
            ->where('message', 'LIKE', '%' . $registratieAanvragen . '%')
            ->where('date', $date)
            ->count();
    }

    public function accountAanmaken($date)
    {
        $registratieAanvragen = '%getEmailFromGuidNameAction%';
        return AareonLog::select('message')
            ->where('message', 'LIKE', '%' . $registratieAanvragen . '%')
            ->where('date', $date)
            ->count();
    }

    public function countRegistration($date)
    {
        $registratieAanvragen = '%UtilisateurNative: inscription%';

        return AareonLog::select('message')
            ->where('message', 'LIKE', '%' . $registratieAanvragen . '%')
            ->where('date', $date)
            ->count();
    }

    public function getActivatedPerDay($date)
    {
        return KprUser::where('DATE_ACTIVATION', $date)
            ->count();
    }

    public function getNotActivatedPerDay($date)
    {
        return KprUserActivation::whereRaw('DATE(DATE) = ?', $date)
            ->count();
    }

    public function activerenAccount($date)
    {
        $registratieAanvragen = '%KPR_UTILISATEURS%';
        return AareonLog::select('message')
            ->where('message', 'LIKE', '%' . $registratieAanvragen . '%')
            ->where('date', $date)
            ->count();
    }

    private function getDates()
    {
        return AareonLog::select('date')
            ->groupBy('date')->get()->pluck('date');

    }

    private function getAllRegAanvragen()
    {
        $registratieAanvragen = '%Get params: {"ReferenceType":"%';
        return cache()->remember('all-reg-requests', 60 * 60 * 24,
            function () use ($registratieAanvragen) {
                return AareonLog::selectRaw('date,
        count(*) as Registratie_aanvragen'

                )
                    ->where('message', 'LIKE', '%' . $registratieAanvragen . '%')
                    ->groupBy('date')->get();
            });
    }
}
