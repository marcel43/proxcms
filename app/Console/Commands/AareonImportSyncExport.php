<?php

namespace App\Console\Commands;

use App\Imports\SyncExport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class AareonImportSyncExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aareon:sync:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports the Action output Exportfile';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $file = 'app\import\export.csv';
        $handle = fopen(storage_path($file), 'r');
        Excel::import(new SyncExport(), File::get(storage_path().'/'.$file));
    }

}
