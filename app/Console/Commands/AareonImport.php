<?php

namespace App\Console\Commands;

use App\Models\AareonLog;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

class AareonImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aareon:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import for logfile';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');
        $this->info('fetching lines');
        $lineCount = $this->linecount();
        $progressBar = $this->output->createProgressBar($lineCount);
        $progressBar->setMessage('counting file lines');
        $progressBar->start();
        $this->importFile($progressBar);

        $this->info('clearing related cache');
        cache()->delete('all-reg-requests');
        $this->info('Done');

    }

    private function linecount()
    {
        $linecount = 0;
        $file = 'app/import/upload.log';
        $handle = fopen(storage_path($file), 'r');
        while(!feof($handle)){
            $line = fgets($handle);
            $linecount++;
        }

        fclose($handle);

        return $linecount;
    }

    private function cleanUp($fread) {
        $remove = "\n";

        return explode($remove, $fread);
    }

    private function importFile($bar)
    {
        $file = 'app/import/upload.log';
        $handle = fopen(storage_path($file), 'r');
        if (!$handle) throw new Exception();
        $fread = fread($handle,filesize(storage_path($file)));
        fclose($handle);
        $clean = $this->cleanUp($fread);
        $this->info('starting import');

        foreach ($clean as $lineNumber => $line) {
            try {
                if($this->splitLine($lineNumber, $line)) {
                }
            }catch (Exception $e) {
                $this->info($lineNumber.' has failed');
            }

            $bar->advance();
        }
    }

    private function splitLine($lineNo,$line)
    {

        $tab = "\t";
        $values = explode($tab, $line);

        //$log = new AareonLog();
        $log = [];
        if (count($values) > 1) {
            $lineHandle = $values[0];
            unset($values[0]);
            $message = implode(';', $values);
        } else {
            $lineHandle = $values[0];
        }
        $store = true;
        //Determine date;
        $log['date'] = substr($lineHandle, 0, 10);
        $log['time'] = substr($lineHandle, 11, 8);
        //Determine type
        if (substr($lineHandle, 26, 4) === 'INFO') {
            $log['type'] = substr($lineHandle, 26, 4);
            $start = 36;
        } elseif (substr($values[0], 26, 6) === 'NOTICE') {
            $start = 38;
            $log['type'] = substr($lineHandle, 26, 6);
        } else {
            if (array_search($lineNo, ['963'], false)) {
                dd([$lineHandle, $lineNo, 'test', substr($values[0], 27, 4)]);
            } else {
                $store = false;
                $start = 39;
            }
        }

        //storing the message
        if (isset($message)) {
            $log['message'] = $message;
        } else {
            $log['message'] = substr($lineHandle, $start);
        }
        try {
            if($store)
                if (!AareonLog::where($log)->exists()
                ) {
                    AareonLog::create($log);
                }else {
                    return true;
                }

        }catch (Exception $e) {
            dd([$log, $lineNo]);
        }
        return false;
    }
}
