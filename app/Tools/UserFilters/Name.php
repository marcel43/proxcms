<?php


namespace App\Tools\UserFilters;


use App\Tools\FilterContract;

class Name implements FilterContract
{
    protected $query;

    /**
     * Name constructor.
     * @param $query
     */
    public function __construct($query)
    {
        $this->query = $query;
    }

    /**
     * @param $value
     */
    public function handle($value): void
    {
        $this->query->where('name', 'LIKE', '%'.$value.'%');
    }
}
