<?php


namespace App\Tools;

/**
 * Interface FilterContract
 * @package App\Tools
 */
interface FilterContract
{
    public function handle($value): void;
}
