<?php


namespace App\Constants;


use Illuminate\Support\Collection;

class FilterNames
{
    /**
     * @return Collection
     */
    public static function parameters(): Collection
    {
        return collect([
            'App\Tools\UserFilters' => [
                'name' => [
                    'text' => 'Name', // Displays the name of the filter
                    'value' => 'name', // Fills the correct parameter
                    'output' => 'string' // Helps with type of filter
                ],
                'email' => [
                    'text' => 'Email',
                    'value' => 'email',
                    'output' => 'string'
                ]
            ]
        ]);
    }
}
