<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class OutputImport extends Model
{
    use HasFactory;
    protected $fillable = [
        'foldername',
        'UserRefId',
        'DateTimeAction',
        'DateTimeXml',
    ];

    protected $with = [
        'Export',
        'ExportInverse'
    ];

    protected $appends = [
        'SyncTime',
        'ExportTime',
        'ImportTime',
        'Hour',
        'DiffActionXml'
    ];

    /**
     * @return string
     */
    public function getHourAttribute()
    {
        return Carbon::parse($this->DateTimeXml)->format('H');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('DateTimeXml', 'asc')->get();
    }

    /**
     * @return string
     */
    public function getExportTimeAttribute(){
        return Carbon::parse($this->Export->DateTimeXml)->toTimeString();
    }

    /**
     * @return string
     */
    public function getImportTimeAttribute(){
        return Carbon::parse($this->DateTimeAction)->toTimeString();
    }

    /**
     * @return float|int
     */
    public function getSyncTimeAttribute()
    {
        $startTime = Carbon::createFromFormat('Y-m-d H:i:s', $this->DateTimeXml);
                $finishTime = Carbon::createFromFormat('Y-m-d H:i:s',$this->Export
                ()->where('DateTimeXml' ,'>', $this->DateTimeXml)->first()
                    ->DateTimeXml)
                ;
        return $startTime->diffInSeconds($finishTime);
    }

    /**
     * @return mixed
     */
    public function getRegAndTimeSyncAttribute()
    {
        return $this->SyncTime + $this->DiffActionXml;
    }

    /**
     * @return float|int
     */
    public function getDiffActionXmlAttribute(){
        $startTime = Carbon::parse($this->DateTimeAction);
        $finishTime = Carbon::parse($this->DateTimeXml);
        $exportXmlTime= Carbon::parse($this->Export->DateTimeXml);
        return $startTime->diffInSeconds($finishTime);
    }

    /**
     * @return HasOne
     */
    public function Export()
    {
        return $this->hasOne(OutputExport::class, 'UserRefId', 'UserRefId')
            ->orderBy('DateTimeXml', 'ASC');
    }

    /**
     * @return HasOne
     */
    public function ExportInverse()
    {
        return $this->hasOne(OutputExport::class, 'UserRefId', 'UserRefId')
            ->orderBy('DateTimeXml', 'DESC')->latest();
    }

}
