<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AareonLogDay extends Model
{
    use HasFactory;

    protected $fillable = [
        'datum',
        'reg_code_requests',
        'reg_code_requests_unique',
        'reg_code_requests_mail',
        'account_reg_create',
        'account_mail_received',
        'activated',
        'not_activated',
    ];
}
