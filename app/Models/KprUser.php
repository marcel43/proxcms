<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class KprUser extends Model
{
    /**
     * The Connection of the database
     *
     * @var String
     */
    protected $connection = "mysql_tunnel";

    protected $table = "KPR_UTILISATEURS";

    protected $attributes  = [
        'amount'
    ];

    public function setAmountAttriubute($value)
    {
        $this->attributes['amount'] = $value;
    }

    public function getAmountAttribute()
    {
        return $this->attributes['amount'];
    }

}
