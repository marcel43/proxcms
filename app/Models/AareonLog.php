<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AareonLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'date',
        'time',
        'message',
        'call',
        'pid',
        'type'
    ];

    public static function getAVGHourReport()
    {
        $registratieAanvragen = '%getEmailFromGuidNameAction%';
        $registratieAanvragen2 = '%UtilisateurNative: inscription%';
        return cache()->remember('all-reg-avg-report', 60 * 60 * 24,
            function () use($registratieAanvragen2, $registratieAanvragen) {
                return AareonLog::selectRaw('the_hour,CEILING(avg(the_count)) as amount, the_message')
                    ->fromSub(function ($query) use ($registratieAanvragen, $registratieAanvragen2) {
                        $query
                            ->selectRaw('
                date,
                hour(time) as the_hour,
                count(*) as the_count,
                CASE
                WHEN LEFT(message , 5) = "Perso" THEN "Email Ontvangen"
                WHEN LEFT(message , 4) = "Util" THEN "Account geregistreerd"
                ELSE LEFT(message , 5)
                END AS the_message

                ')
                            ->distinct('message')
                            ->where('message', 'LIKE', '%' . $registratieAanvragen . '%')
                            ->orWhere('message', 'LIKE', '%' . $registratieAanvragen2 . '%')
                            ->from('aareon_logs')
                            ->groupBy('the_hour', 'date', 'the_message');
                    }, 's')
                    ->groupBy('the_message', 'the_hour')->get();
            });
    }

    public static function getAVGDayReport()
    {
        $registratieAanvragen = '%getEmailFromGuidNameAction%';
        $registratieAanvragen2 = '%UtilisateurNative: inscription%';
        return cache()->remember('all-reg-avg-day-report', 60 * 60 * 24,
            function () use($registratieAanvragen2, $registratieAanvragen) {
                return AareonLog::selectRaw('the_day,CEILING(avg(the_count)) as amount, the_message')
                    ->fromSub(function ($query) use ($registratieAanvragen, $registratieAanvragen2) {
                        $query
                            ->selectRaw('
                date,
                CASE
                WHEN WEEKDAY(date) = 0 THEN "Maandag"
                WHEN WEEKDAY(date) = 1 THEN "Dinsdag"
                WHEN WEEKDAY(date) = 2 THEN "Woensdag"
                WHEN WEEKDAY(date) = 3 THEN "Donderdag"
                WHEN WEEKDAY(date) = 4 THEN "Vrijdag"
                WHEN WEEKDAY(date) = 5 THEN "Zaterdag"
                WHEN WEEKDAY(date) = 6 THEN "Zondag"
                END as the_day,
                count(*) as the_count,
                CASE
                WHEN LEFT(message , 5) = "Perso" THEN "Email Ontvangen"
                WHEN LEFT(message , 4) = "Util" THEN "Account geregistreerd"
                ELSE LEFT(message , 5)
                END AS the_message,
                WEEKDAY(date) as day
                           ')
                            ->distinct('message')
                            ->where('message', 'LIKE', '%' . $registratieAanvragen . '%')
                            ->orWhere('message', 'LIKE', '%' . $registratieAanvragen2 . '%')
                            ->from('aareon_logs')
                            ->groupBy('the_day', 'date', 'the_message');
                    }, 's')
                    ->orderBy('day')
                    ->groupBy('the_day', 'the_message', 'day')->get();
            });
    }
}
