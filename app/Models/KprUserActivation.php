<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class KprUserActivation extends Model
{
    /**
     * The Connection of the database
     *
     * @var String
     */
    protected $connection = "mysql_tunnel";

    protected $table = "KPR_ACTIVATION_UTILISATEURS";


}
