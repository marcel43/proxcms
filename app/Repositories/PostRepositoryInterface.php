<?php


namespace App\Repositories;


interface PostRepositoryInterface
{
    /**
     * Gets a post by it's ID
     *
     * @param $post_id
     * @return int
     */
    public function get($post_id);

    /**
     * Get's all posts
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes a Post
     *
     * @param int
     */
    public function delete($post_id);

    /**
     * Updates a post.
     *
     * @param int
     * @param array
     */
    public function update($post_id, array $post_data);
}
