<?php


namespace App\Repositories\Redmine;


use App\Repositories\PostRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Redmine\Client\NativeCurlClient;

class RedmineIssueRepository implements PostRepositoryInterface
{
    private $client;

    public function __construct() {
        $this->client = new NativeCurlClient(
            'https://crmsvn01.rz.aareon.com/',
            auth()->user()->redmine_token
        );
    }

    public function get($post_id)
    {
        $this->client->getApi('issue')->all();
    }

    public function all()
    {
        $this->client->getApi('issue')->all();
    }

    public function delete($post_id)
    {
        // TODO: Implement delete() method.
    }

    public function update($post_id, array $post_data)
    {
        // TODO: Implement update() method.
    }
}
