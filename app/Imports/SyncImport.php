<?php

namespace App\Imports;

use App\Models\OutputExport;
use App\Models\OutputImport;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Validators\Failure;
use PHPUnit\Exception;

class SyncImport implements ToModel, WithCustomCsvSettings, SkipsOnFailure, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if(!in_array(null, $row, true)) {
            if (!OutputImport::where([
                    'foldername' => preg_replace('/[^\PC\s]|\n/u', '', $row[0]),
                    'UserRefId' => preg_replace('/[^\PC\s]|\n/u', '', $row[1]),
                'DateTimeAction' => $this->cleanTimeStamp($row[2]),
                'DateTimeXml' => $this->cleanTimeStamp($row[3]),
                ])->exists())
            return new OutputImport([
                'foldername' => preg_replace('/[^\PC\s]|\n/u', '', $row[0]),
                'UserRefId' => preg_replace('/[^\PC\s]|\n/u', '', $row[1]),
                'DateTimeAction' => $this->cleanTimeStamp($row[2]),
                'DateTimeXml' => $this->cleanTimeStamp($row[3]),
            ]);
        }

    }
    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
    /**
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        dd($failures);
    }

    private function cleanTimeStamp($value) {
        $hour = substr(preg_replace('/[^\PC\s]/u', '', $value), 20, 2);
        $timestamp = substr(preg_replace('/[^\PC\s]/u', '', $value), 0, 19);
        $carbonStamp = Carbon::parse($timestamp);
        if(!$hour) {
            return $carbonStamp->toDateTimeString();
        } else {
            return $carbonStamp->addHours($hour)->toDateTimeString();
        }

    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => "\t"
        ];
    }
    private function splitTimeStamp($value, $stamp = 'date') {
        $value = preg_replace('/[^\PC\s]/u', '', $value);
        if($stamp === 'time') {
            $time = Carbon::createFromFormat('Y-m-d H:i:s',$value);
            return $time->format('H:m:s');
        }
        $date =  Carbon::createFromFormat('Y-m-d H:i:s',$value);
        return $date->format('Y-m-d');
    }
}
