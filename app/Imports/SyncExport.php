<?php

namespace App\Imports;

use App\Models\OutputExport;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Validators\Failure;
use PHPUnit\Exception;

class SyncExport implements ToModel, WithCustomCsvSettings, SkipsOnFailure, WithStartRow, ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if(!in_array(null, $row, true)) {
            if (!OutputExport::where([
                    'foldername' => preg_replace('/[^\PC\s]|\n/u', '', $row[0]),
                    'UserRefId' => preg_replace('/[^\PC\s]/u', '', $row[1]),
                    'DateTimeXml' => $this->cleanTimeStamp($row[2]),
                ])->exists()){
                return new OutputExport([
                    'foldername' => preg_replace('/[^\PC\s]|\n/u', '', $row[0]),
                    'UserRefId' => preg_replace('/[^\PC\s]|\n/u', '', $row[1]),
                    'DateTimeXml' => $this->cleanTimeStamp($row[2]),

                ]);
            }

        }

    }

    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {

    }
    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
    /**
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        dd($failures);
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => "\t"
        ];
    }
    private function cleanTimeStamp($value) {
        $value = preg_replace('/[^\PC\s]/u', '', $value);
        return Carbon::createFromFormat('Y-m-d H:i:s', $value)->addHours(2)->toDateTimeString();
    }
}
