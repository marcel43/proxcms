<?php


namespace App\Traits;


use App\Constants\FilterNames;
use App\Tools\FilterBuilder;

Trait FilterTrait
{
    /**
    * @param $query
    * @param $filters
    * @return mixed
    */
    public function scopeFilterBy($query, $filters)
    {
        $namespace = self::filterNameSpace;
        $filter = new FilterBuilder($query, $filters, $namespace);

        return $filter->apply();
    }

    public static function filterNames() {
        return FilterNames::parameters()->get(self::filterNameSpace);
    }
}
