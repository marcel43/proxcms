<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAareonLogDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aareon_log_days', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->date('datum');
            $table->integer('reg_code_requests');
            $table->integer('reg_code_requests_unique');
            $table->integer('reg_code_requests_mail');
            $table->integer('account_reg_create');
            $table->integer('account_mail_received');
            $table->integer('activated');
            $table->integer('not_activated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aareon_log_days');
    }
}
