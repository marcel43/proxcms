<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAareonLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aareon_logs', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->date('date');
            $table->text('message')->nullable();
            $table->json('call')->nullable();
            $table->bigInteger('pid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aareon_logs');
    }
}
