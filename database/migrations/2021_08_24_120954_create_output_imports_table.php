<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutputImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('output_imports', function (Blueprint $table) {
            $table->id();
            $table->string('foldername');
            $table->integer('UserRefId')->index();
            $table->dateTime('DateTimeAction');
            $table->dateTime('DateTimeXml');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('output_imports');
    }
}
