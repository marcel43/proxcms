<span class="
    text-xs
    font-semibold
    inline-block
    py-1 px-2
    uppercase rounded text-green-900 bg-green-600 uppercase last:mr-0 mr-1">
  {{ $slot  }}
</span>
