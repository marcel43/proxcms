<div>
    <form wire:submit.prevent="save">
        <input type="file" wire:model="import">

        @error('import') <span class="error">{{ $message }}</span> @enderror

        <button type="submit">Save Import</button>
    </form>
</div>
