<div class="bg-white flex-inline m-1 p-2 overflow-hidden shadow sm:rounded-sm flex">
    {{ $slot }}
</div>
