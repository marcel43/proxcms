<div>
    <div class="lg:w-1/5 py-1 text-sm font-medium sm:px-0 float-left lg:px-0">
        {{ $slot }}
    </div>
</div>
