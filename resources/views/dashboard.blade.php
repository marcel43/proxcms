    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
           <x-card>
            {{--       Displays registered total per day Table       --}}
               <div class="w-1/5">
                   <livewire:dashboard.table />
               </div>
               <div class="w-4/5">
                   <livewire:dashboard.chart />
               </div>


           </x-card>
            <x-card>
                <livewire:log-report />
           </x-card>

        </div>
    </div>
