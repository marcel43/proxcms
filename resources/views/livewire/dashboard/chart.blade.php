<div class="shadow rounded p-4 border bg-white flex-1" style="height: 16rem;">
    <label>
        <input type="checkbox" wire:model="byweek" />{{ $firstRun }} Show by week
    </label>
    <livewire:livewire-line-chart
        key="{{ $columnChartModel->reactiveKey()}}"
        :line-chart-model="$columnChartModel"
    />
</div>
