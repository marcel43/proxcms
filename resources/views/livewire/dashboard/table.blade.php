<div>
    <label>
        <input type="checkbox" wire:model="byweek" /> Show by week
    </label>
    <table class="table-auto border-2 bg-blue-700 text-sm font-medium">
        <thead class="text-center text-white">
        <tr>
            <th class="px-8">Datum</th>
            <th class="px-4">Aantal</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $row)

            <tr class="border-2 bg-white">
                <td class="border-2 text-center">
                    {{ ($byweek)?$row['datum']
                        :
                           Carbon\Carbon::parse($row['datum'])->format('j-n-Y') }}
                </td>
                <td class="border-2 text-center">{{$row['aantal']}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
