<th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
<span class="inline-block align-text-middle w-full">
    {{ $column }}
    @if($filter)
        <input
            class="w-3/6 px-2 py-1 float-right placeholder-blueGray-500 text-blueGray-600 relative bg-white bg-white rounded text-sm border-0 shadow outline-none focus:outline-none focus:ring"
            type="text"
            name="{{$filter['value']}}"
            placeholder="{{$filter['text']}}"
            wire:model.debounce="{{ $filter['value']}}"
        />
    @endif
</th>
