<div class="m-2 w-1/5">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
        Search Users
    </label>
    <input wire:model.debounce="search"
           class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
           id="search" name="search" type="text" placeholder="Search value"
    />
</div>
