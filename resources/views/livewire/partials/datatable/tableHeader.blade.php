<thead class="bg-gray-50">
<tr>
    @foreach ($columns as $column)
        @include('livewire.partials.datatable.columnHeader', [
            'column' => $column,
            'filter' => $filters[$column]?? false
        ])
    @endforeach
    @include('livewire.partials.datatable.tableHeaderOptions')
</tr>
</thead>
