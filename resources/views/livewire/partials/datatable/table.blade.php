<table class="min-w-full divide-y divide-gray-200">
    @include('livewire.partials.datatable.tableHeader')
    <t-body class="bg-white divide-y divide-gray-200">
        @foreach ($resources as $resource)
            <tr class="border-2 border-bottom">
                @foreach($columns as $column)
                <td class="px-6 py-4 whitespace-nowrap">{{ $resource->$column }}</td>
                @endforeach
                <td class="px-6 py-4 whitespace-nowrap" colspan="2">
                    <button>some option</button>
                </td>
            </tr>
        @endforeach
    </t-body>
</table>
