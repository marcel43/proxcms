<div
    x-data="{ isUploading: false, progress: 0 }"
    x-on:livewire-upload-start="isUploading = true"
    x-on:livewire-upload-finish="isUploading = false"
    x-on:livewire-upload-error="isUploading = false"
    x-on:livewire-upload-progress="progress = $event.detail.progress"
>
    <form wire:submit.prevent="save">
        <input type="file" wire:model="import">
        <div wire:loading wire:target="import"><x-badge>Uploading...</x-badge></div>
        @error('import.*') <span class="error">{{ $message }}</span> @enderror

        <div x-show="isUploading">
            <progress max="100" x-bind:value="progress"></progress>
        </div>
        <div x-show="!isUploading">
            <button x-show="$wire.canAcces"
                type="submit"
                class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
                <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M13 8V2H7v6H2l8 8 8-8h-5zM0 18h20v2H0v-2z"/></svg>
                <span>Process File</span>
            </button>
        </div>
        <div wire:loading>
            running
        </div>
    </form>
</div>
