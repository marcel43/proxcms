<div>
    {{ $date }}
    <x-card>
        <table  class="table-auto border-2 bg-blue-700 text-sm font-medium">
            <thead class="border text-center text-white">
            <tr class="">
                <th class="border px-1">Uur</th>
                <th class="border px-1">Inschrijvingen</th>
                <th class="border px-1">Gem. duur synchronisatie</th>
                <th class="border px-1">Maximum synchronisatietijd</th>
                <th class="border px-1">Gem. duur sync+reg.tijd</th>
                <th class="border px-1">Max. duur sync+reg tijd</th>
                <th class="border px-1">Verschil tussen XML en actie tijd</th>
            </tr>
            </thead>
            <tbody class="text-center border">
            @isset($tableInfo)
                @foreach($tableInfo as $row)
                    <tr  class="border-2 bg-white">
                        <td class="border">{{$row['hour']}}</td>
                        <td class="border">{{$row['count']}}</td>
                        <td class="border">{{gmdate('H:i:s',$row['avgSync'])}}</td>
                        <td class="border">{{gmdate('H:i:s',$row['maxSync'])}}</td>
                        <td class="border">{{gmdate('H:i:s',$row['RegAndTimeSync'])}}</td>
                        <td class="border">{{gmdate('H:i:s',$row['maxRegAndTimeSync'])}}</td>
                        <td class="border">{{gmdate('H:i:s',$row['avgDiffActionXml'])}}</td>
                    </tr>
                @endforeach
                <tfoot class="border text-center text-white">
                <tr>
                    <th class="border px-1">EindTotaal</th>
                    <th class="border px-1">{{ $tableInfo->sum('count') }}</th>
                    <th class="border px-1">{{ gmdate('H:i:s',$tableInfo->avg('avgSync')) }}</th>
                    <th class="border px-1">{{ gmdate('H:i:s',$tableInfo->max('maxSync')) }}</th>
                    <th class="border px-1">{{ gmdate('H:i:s',$tableInfo->avg('RegAndTimeSync')) }}</th>
                    <th class="border px-1">{{ gmdate('H:i:s',$tableInfo->max('maxRegAndTimeSync')) }}</th>
                    <th class="border px-1">{{ gmdate('H:i:s',$tableInfo->avg('avgDiffActionXml')) }}</th>
                </tr>
                </tfoot>
            @endisset
            </tbody>
        </table>
    </x-card>
    <div class="shadow rounded p-4 border bg-white w-3/6" style="height: 32rem;">
            <livewire:livewire-line-chart
                key="{{ $chart->reactiveKey()}}"
                :line-chart-model="$chart"
            />
        </div>
</div>
