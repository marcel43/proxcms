<div>
    <div>
        <table class="table-auto border-2 bg-blue-700 text-sm font-medium">
            <thead class="text-center text-white">
            <tr>
                <th class="px-8">Output Type</th>
                <th class="px-8">records</th>
            </tr>
            </thead>
            <tbody>
                @foreach($data as $row)
                    <tr class="border-2 bg-white">
                        <td class="border-2">{{ $row['name'] }}</td>
                        <td class="border-2">{{ $row['count'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
