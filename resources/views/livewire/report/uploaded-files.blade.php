<div>
    <div>
        <table class="table-auto border-2 bg-blue-700 text-sm font-medium">
            <thead class="text-center text-white">
            <tr>
                <th class="px-8">Filename</th>
                <th class="px-8">records</th>
                <th class="px-8">Date</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $row)
                <tr class="border-2 bg-white">
                    <td class="border-2">{{ $row->filename }}</td>
                    <td class="border-2">{{ $row->records }}</td>
                    <td class="border-2">{{ \Carbon\Carbon::parse($row->created_at)->toDateString() }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
