<div>
    <table class="border table-auto">
        <thead>
            <tr><th colspan="8"  class="text-center">{{ $today }}</th></tr>
            <tr><th colspan="8"  class="text-center">{{ $dateSet }}</th></tr>
            <tr><td><<</td><td><</td><td colspan="4" class="text-center"> {{ $year }} </td><td>></td><td>>></td></tr>
            <tr>
                <td><<</td>
                <td><a wire:click="decreaseMonth" href="#"><</a></td>
                <td colspan="4"  class="text-center">{{ $month }}</td>
                <td class="p-1"><a wire:click="increaseMonth" href="#" class="hover:bg-indigo-500 pointer-event">></a></td>
                <td>>></td>
            </tr>
            <tr>
                <th class="p-1 border border-black ">WK</th>
                <th class="p-1 border border-black ">Ma</th>
                <th class="p-1 border border-black ">Di</th>
                <th class="p-1 border border-black ">Wo</th>
                <th class="p-1 border border-black ">Do</th>
                <th class="p-1 border border-black ">Vr</th>
                <th class="p-1 border border-black bg-gray-200">Za</th>
                <th class="p-1 border border-black bg-gray-200">Zo</th>
            </tr>
        </thead>
        <tbody>
        @foreach($calendar as $key => $week)
            <tr>
                <td class="p-1 border border-black ">{{$week['number']}}</td>
                @foreach($week['days'] as $dayIndex => $day)
                    @if($day['isCurrent'])
                        <td class="border border-black"><a wire:click="dataSelect({{$key}} , {{$dayIndex}})" class="bg-blue-500 p-1 rounded text-white" href="#">{{ \Carbon\Carbon::parse($day['date'])->format('d') }}</a></td>
                    @else
                        <td class="border border-black {{($day['hasData'])? 'bg-green-200': 'bg-red-50'}} {{($day['isWeekend'])? 'bg-gray-200': ''}}"><a class="p-1" wire:click="dataSelect({{$key}} , {{$dayIndex}})" href="#"> {{ \Carbon\Carbon::parse($day['date'])->format('d') }}</a></td>
                    @endif()

                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
