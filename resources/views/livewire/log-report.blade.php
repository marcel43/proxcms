<div>
    <label>
    <input type="checkbox" wire:model="byweek" /> Show by week
    </label>

    <table class="table-fixed border-2 bg-blue-700 text-sm font-medium">
        <thead class="text-left text-white">
            <tr>
                <th colspan="6" class="bg-black">Uit de log files</th>
                <th colspan="2" class="bg-green-500">Uit de database</th>
            </tr>
            <tr class="h-40">
                <th class="transform md:rotate-90 w-28 h-12">
                   @if($byweek)Week @else Datum @endif</th>
                <th class="transform md:rotate-90 w-28 h-12">
                    Registratiecode aanvraag</th>
                <th class="transform md:rotate-90 w-28 h-12">
                    Registratiecode aanvraag uniek</th>
                <th class="transform md:rotate-90 w-28 h-12">
                    Mail met regcode</th>
                <th class="transform md:rotate-90 w-28 h-12">
                   Account aanmaken totaal gelukt/niet gelukt</th>
                <th class="transform md:rotate-90 w-28 h-12">
                    Registratie gelukt, mail ontvangen, niet uniek</th>
                <th class="transform md:rotate-90 w-28 h-12">
                    Activeren</th>
                <th class="transform md:rotate-90 w-28 h-12">
                   Niet geactiveerd</th>
            </tr>
        </thead>
        <tbody class="border-2 bg-white">
            @foreach($report as $event)
                <tr>
                    <td class="border-2 text-center">{{ $byweek? $event['week']: \Carbon\Carbon::parse($event['datum'])->format('Y-m-d') }}</td>
                    <td class="border-2 text-center">{{ $event['reg_code_requests'] }}</td>
                    <td class="border-2 text-center">{{ $event['reg_code_requests_unique'] }}</td>
                    <td class="border-2 text-center">{{ $event['reg_code_requests_mail'] }}</td>
                    <td class="border-2 text-center">{{ $event['account_reg_create'] }}</td>
                    <td class="border-2 text-center">{{ $event['account_mail_received'] }}</td>
                    <td class="border-2 text-center">{{ $event['activated'] }}</td>
                    <td class="border-2 text-center">{{ $event['not_activated'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
