<div>
    <div class="flex flex-col">
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Dashboard / Users') }}
            </h2>
        </x-slot>
        <div class="-my-2 w-full p-4">
            <div class="py-2 align-middle inline-block sm:px-8 lg:px-11 w-full">
                <div class="shadow overflow-hidden border-b border-gray-200 bg-white p-2 sm:rounded-lg w-full">
                    @include('livewire.partials.datatable.search')
                    <div class="m-5">
                        {{ $resources->links() }}
                    </div>
                    @include('livewire.partials.datatable.table')
                    <div class="m-5">
                        {{ $resources->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
