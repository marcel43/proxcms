<div>
    <div class="py-1 px-1 h-full overflow-auto">
        <div class="lg:w-1/5 py-1 text-sm font-medium sm:px-0 float-left lg:px-0">
            <x-card>
                <h3>File upload</h3>
                <p>
                    Please upload the log-file...
                </p>
                <livewire:actions.upload-import />
            </x-card>
            <x-card>
                <h3>Status file import</h3>
                <p>Number of records will be reset when record is uploaded</p>
                <p>Available Records: <x-badge>{{$totalRecordsDB}}</x-badge></p>
            </x-card>
            <x-card>
                <h3>php information</h3>
                version {{ phpversion() }} <br/>
                maxfile upload  {{ ini_get('max_file_uploads') }}<br/>
                maxfile upload size {{ ini_get('upload_max_filesize') }}<br/>
                max upload POST size {{ ini_get('post_max_size') }}<br/>
                max mem script {{ ini_get('memory_limit') }}<br/>
            </x-card>
            <x-card>
                <livewire:livewire-line-chart
                    key="{{ $avgPerHourChart->reactiveKey() }}"
                    :line-chart-model="$avgPerHourChart"
                />
            </x-card>
            <x-card>
                <livewire:livewire-line-chart
                    key="{{ $avgPerDayChart->reactiveKey() }}"
                    :line-chart-model="$avgPerDayChart"
                />
            </x-card>
        </div>
        <div class="lg:w-4/5 sm:px-0 lg:px-0 float-left">
            <x-card>
                <livewire:log-report />
            </x-card>
        </div>
    </div>
</div>

