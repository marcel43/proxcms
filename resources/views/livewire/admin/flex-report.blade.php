<div class="py-1 px-1 h-full overflow-auto">
    <x-fivecolumn>
        <x-card>
            <livewire:actions.upload-import />
        </x-card>
        <x-card>
            <livewire:date-selector />
        </x-card>
        <x-card>
            <livewire:report.flex-status />
        </x-card>
        <x-card>
            <livewire:report.uploaded-files />
        </x-card>

    </x-fivecolumn>
    <x-fivecolumndetail>
            <livewire:report.index-overview />
    </x-fivecolumndetail>
</div>
